package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;
import java.util.List;

public class BookRepository {

    private MutableLiveData<Book> selectedBook =
            new MutableLiveData<>();

    private LiveData<List<Book>> allBooks;

    private BookDao bookDao;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        allBooks = bookDao.getAllBooks();
    }

    public void insertBook(Book book){
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(book);
        });
    }

    public void updateBook(Book book) {
        databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(book);
        });
    }



    public LiveData<List<Book>> getAllBooks()
    {
        return allBooks;
    }

    public MutableLiveData<Book> getSelectedBooks()
    {
        return selectedBook;
    }
}
