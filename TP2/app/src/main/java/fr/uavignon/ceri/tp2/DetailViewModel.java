package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {

    private BookRepository repository;
    MutableLiveData<Book> selectedBook;

    public DetailViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
        selectedBook = repository.getSelectedBooks();
    }

    public void updateOrInsertBook(Book book)
    {
        if(book.getId()<0)
            repository.insertBook(book);
        else
            repository.updateBook(book);
    }

    MutableLiveData<Book> getSelectedBook(){return selectedBook;}

}
